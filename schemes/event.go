package schemes

import (
	"time"

	"gitlab.com/weepi/common/db/postgres/schemes"

	"github.com/satori/go.uuid"
)

type Event struct {
	schemes.Model
	Private          bool `gorm:"not null;default:false"`
	Sponsored        bool `gorm:"not null;default:false"`
	Title            string
	StartDate        time.Time
	EndDate          time.Time
	EndDateTimestamp uint64
	Description      string
	Latitude         float64
	Longitude        float64
	SportUUID        uuid.UUID          `gorm:"type:uuid"`
	MinParticipants  uint32             `gorm:"not null;default:0"`
	MaxParticipants  uint32             `gorm:"not null;default:0"`
	OwnerUUID        uuid.UUID          `gorm:"type:uuid"`
	EventParticipant []EventParticipant `gorm:"ForeignKey:EventUUID"`
}
