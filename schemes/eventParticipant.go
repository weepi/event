package schemes

import "github.com/satori/go.uuid"

type EventParticipant struct {
	EventUUID       uuid.UUID `sql:"type:uuid REFERENCES events(uuid)"`
	ParticipantUUID uuid.UUID
}
