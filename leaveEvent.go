package main

import (
	"errors"
	"time"

	pb "gitlab.com/weepi/event/proto"
	"gitlab.com/weepi/event/schemes"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
)

var (
	ErrNotInEvent             = errors.New("you are not in this event")
	ErrCannotLeave            = errors.New("error when trying to leave the event")
	ErrNotAllowedToLeaveEvent = errors.New("the owner cannot leave the event")
	ErrInvalidUserUUID        = errors.New("The user uuid is invalid")
)

func (s *WeepiEvent) LeaveEvent(ctx context.Context, in *pb.UserEvent) (*empty.Empty, error) {
	// Checking validity of uuid
	eventUUID, err := uuid.FromString(in.GetEventUUID())
	if err != nil {
		return nil, ErrInvalidEventUUID
	}
	userUUID, err := uuid.FromString(in.GetUserUUID())
	if err != nil {
		return nil, ErrInvalidUserUUID
	}

	// Getting event from db
	event := schemes.Event{}
	if s.db.Where("uuid = ?", eventUUID).First(&event).RecordNotFound() {
		Logger.Errorf("Failed to get event: %s does not exist in DB", eventUUID)
		return nil, ErrInvalidDb
	}

	// Deleting eventParticipant entry if existing
	if s.db.Where("participant_uuid = ? AND event_uuid = ?", userUUID, eventUUID).First(&schemes.EventParticipant{}).RecordNotFound() {
		Logger.WithError(ErrRecordNotFound).Error("User not in event")
		return nil, ErrNotInEvent
	} else {
		if !s.db.Where("owner_uuid = ? AND uuid = ?", userUUID, eventUUID).First(&schemes.Event{}).RecordNotFound() {
			return nil, ErrNotAllowedToLeaveEvent
		}
		if err := s.db.Where("participant_uuid = ? AND event_uuid = ?", userUUID, eventUUID).
			First(&schemes.EventParticipant{}).Delete(schemes.EventParticipant{}).Error; err != nil {
			Logger.WithError(err).Error("The user could not leave the event")
			return nil, ErrCannotLeave
		}
		var ret error
		if err := s.msg.Request("algolia.event.leave", &eventUUID, &ret, 5*time.Second); err != nil {
			Logger.WithError(err).Error("error requesting event leave")
			return nil, err
		}
		if ret != nil {
			return nil, ret
		}
	}

	return &empty.Empty{}, nil
}
