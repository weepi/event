package main

import (
	"errors"

	"gitlab.com/weepi/common/db"
	pb "gitlab.com/weepi/event/proto"
	"gitlab.com/weepi/event/schemes"

	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
)

func castEvent(db *db.DB, dbEvent schemes.Event) *pb.EventReply {
	pbEvent := &pb.Event{
		Title:     dbEvent.Title,
		UUID:      dbEvent.UUID.String(),
		Private:   dbEvent.Private,
		Sponsored: dbEvent.Sponsored,
		StartDate: &timestamp.Timestamp{
			Seconds: dbEvent.StartDate.Unix(),
		},
		EndDate: &timestamp.Timestamp{
			Seconds: dbEvent.EndDate.Unix(),
		},
		Description:     dbEvent.Description,
		Latitude:        dbEvent.Latitude,
		Longitude:       dbEvent.Longitude,
		SportUUID:       dbEvent.SportUUID.String(),
		MinParticipants: dbEvent.MinParticipants,
		MaxParticipants: dbEvent.MaxParticipants,
		OwnerUUID:       dbEvent.OwnerUUID.String(),
	}

	pbReply := pb.EventReply{
		Event: pbEvent,
	}
	for _, related := range dbEvent.EventParticipant {
		pbReply.Participants = append(pbReply.Participants, &pb.UUID{
			UUID: related.ParticipantUUID.String(),
		})
	}

	return &pbReply
}

func (s *WeepiEvent) EventInfos(ctx context.Context, in *pb.UUID) (*pb.EventReply, error) {
	eventUUID, err := uuid.FromString(in.GetUUID())
	if err != nil {
		return nil, errors.New("Erroneous UUID")
	}

	event := schemes.Event{}
	if s.db.Where("uuid = ?", in.GetUUID()).First(&event).RecordNotFound() {
		Logger.WithError(ErrRecordNotFound).Errorf("Failed to get event: %s does not exist in DB", eventUUID)
		return nil, errors.New("This event is not in db")
	}

	s.db.Model(&event).Related(&event.EventParticipant, "EventParticipant")

	return castEvent(s.db, event), nil
}
