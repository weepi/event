package main

import (
	"errors"

	"github.com/satori/go.uuid"
	"gitlab.com/weepi/event/schemes"
)

func (s *WeepiEvent) deleteEvent(eventUUID string) error {

	if err := s.db.Where("event_uuid = ?", eventUUID).Delete(schemes.EventParticipant{}).Error; err != nil {
		Logger.WithError(err).Error("Could not unsubscribe some users from the event")
		return err
	}

	if err := s.db.Where("uuid = ?", eventUUID).Delete(schemes.Event{}).Error; err != nil {
		Logger.WithError(err).Error(ErrNotDeleted)
		return err
	}

	if err := s.deleteInAlgolia(eventUUID); err != nil {
		Logger.WithError(err).Error(ErrNotDeletedAlgolia)
		return err
	}

	return nil
}

func (s *WeepiEvent) deleteUserEvent(userUUID string) error {

	if _, err := uuid.FromString(userUUID); err != nil {
		return errors.New("Erroneous UserUUID")
	}

	events := []schemes.Event{}
	if err := s.db.Where("owner_uuid = ?", userUUID).Find(&events).Error; err != nil {
		Logger.WithError(err).Errorf("Failed to get user %s events", userUUID)
		return errors.New("Failed to get user events")
	}

	for _, event := range events {
		if err := s.deleteEvent(event.UUID.String()); err != nil {
			return err
		}
	}

	if err := s.db.Where("participant_uuid = ?", userUUID).Delete(schemes.EventParticipant{}).Error; err != nil {
		Logger.WithError(err).Error("The user could not be unsubscribed from events he register")
		return err
	}

	return nil
}
