package main

import (
	"flag"
	"google.golang.org/grpc"
	"log"

	"context"
	in "gitlab.com/weepi/event/proto"
	"time"

	pbtimestamp "github.com/golang/protobuf/ptypes/timestamp"
)

var (
	endpointFlag = flag.String("endpoint", "127.0.0.1:1665", "Endpoint to listen to")
)

func init() {
	flag.Parse()
}

func main() {
	// Establish conn with the server
	conn, err := grpc.Dial(*endpointFlag, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Did not connect : %s", err)
	}
	defer conn.Close()

	// New weepi account client
	event := in.NewWeepiEventClient(conn)

	EventInfos := &in.Event{
		UUID:      "038bb317-bf65-42cb-963a-798ded1cea61",
		Private:   false,
		Sponsored: false,
		StartDate: &pbtimestamp.Timestamp{
			Seconds: 788918400,
		},
		EndDate: &pbtimestamp.Timestamp{
			Seconds: 788928400,
		},
		Title:           "Course de velo``",
		Description:     "Seance running a travers Tomsk",
		Latitude:        56.451465,
		Longitude:       84.977011,
		Sport:           "1",
		MinParticipants: 2,
		MaxParticipants: 15,
		OwnerUUID:       "8a075fbb-b830-4c42-951a-aa73818c6c79",
	}

	/*	uuid := &in.UUID{
		UUID: "8a075fbb-b830-4c42-951a-aa73818c6c79",
	}*/

	// If the creation took more than 2 seconds, cancel it #prettyCool
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	res, err := event.UpdateEvent(ctx, EventInfos)
	//res, err := event.DeleteEvent(ctx, uuid)
	if err != nil {
		log.Fatalf("Error creating event : %v", err)
	}
	log.Printf("Result: %s", res)
}
