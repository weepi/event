package main

import (
	"flag"
	"google.golang.org/grpc"
	"log"

	"context"
	in "gitlab.com/weepi/event/proto"
	"google.golang.org/grpc/metadata"
	"time"
)

var (
	endpointFlag = flag.String("endpoint", "127.0.0.1:1668", "Endpoint to listen to")
	tokenFlag    = flag.String("token", "I5v0tqBFZbsIPHCPizkkKDW5DuzpHhX8Oe5Rq+dGDzU=", "User token")
)

func init() {
	flag.Parse()
}

func main() {
	// Establish conn with the server
	conn, err := grpc.Dial(*endpointFlag, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Did not connect : %s", err)
	}
	defer conn.Close()

	// New weepi account client
	event := in.NewWeepiEventClient(conn)

	/*	uuid := &in.UUID{
		UUID: "8a075fbb-b830-4c42-951a-aa73818c6c79",
	}*/

	// If the creation took more than 2 seconds, cancel it #prettyCool
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	md := metadata.Pairs("authorization", *tokenFlag)
	ctx = metadata.NewOutgoingContext(ctx, md)

	eventUuid := in.UUID{
		UUID: "cf9a0002-ab44-47b1-94c0-fda84f15c844",
	}

	res, err := event.JoinEvent(ctx, &eventUuid)
	//res, err := event.DeleteEvent(ctx, uuid)
	if err != nil {
		log.Fatalf("Error creating event : %v", err)
	}
	log.Printf("Result: %s", res)
}
