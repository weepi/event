package main

import (
	"flag"
	"google.golang.org/grpc"
	"log"

	"context"
	in "gitlab.com/weepi/event/proto"
	"time"
)

//pbtimestamp "github.com/golang/protobuf/ptypes/timestamp"

var (
	endpointFlag = flag.String("endpoint", "127.0.0.1:1665", "Endpoint to listen to")
)

func init() {
	flag.Parse()
}

func main() {
	// Establish conn with the server
	conn, err := grpc.Dial(*endpointFlag, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Did not connect : %s", err)
	}
	defer conn.Close()

	// New weepi account client
	event := in.NewWeepiEventClient(conn)

	/*	EventInfos := &in.Event{
		Private:   false,
		Sponsored: false,
		StartDate: &pbtimestamp.Timestamp{
			Seconds: 788918400,
		},
		EndDate: &pbtimestamp.Timestamp{
			Seconds: 788928400,
		},
		Description:     "toto a la plage",
		Latitude:        36.6275999,
		Longitude:       -121.8636868,
		Sport:           "1",
		MinParticipants: 2,
		MaxParticipants: 15,
	}*/

	uuid := &in.UUID{
		UUID: "63673613-ddbd-4acf-9cec-309b1d7c488b",
	}

	// If the creation took more than 2 seconds, cancel it #prettyCool
	ctx, cancel := context.WithTimeout(context.Background(), 4242*time.Second)
	defer cancel()

	//res, err := event.CreateEvent(ctx, EventInfos)
	res, err := event.DeleteEvent(ctx, uuid)
	if err != nil {
		log.Fatalf("Error creating event : %v", err)
	}
	log.Printf("Result: %s", res)
}
