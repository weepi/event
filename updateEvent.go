package main

import (
	"errors"
	"time"

	algolia "gitlab.com/weepi/algolia/event"
	pb "gitlab.com/weepi/event/proto"
	"gitlab.com/weepi/event/schemes"

	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
)

func (s *WeepiEvent) updateInAlgolia(event *schemes.Event, UUID string) error {
	coords := algolia.Coords{
		Lat: event.Latitude,
		Lng: event.Longitude,
	}

	algoliaEvent := algolia.EventMsg{
		Title:           event.Title,
		ObjectID:        UUID,
		Private:         event.Private,
		Sponsored:       event.Sponsored,
		StartDate:       event.StartDate,
		EndDate:         event.EndDate,
		Description:     event.Description,
		Geoloc:          coords,
		SportUUID:       event.SportUUID.String(),
		MinParticipants: event.MinParticipants,
		MaxParticipants: event.MaxParticipants,
		OwnerUUID:       event.OwnerUUID.String(),
	}

	var ret error
	if err := s.msg.Request("algolia.event.update", &algoliaEvent, &ret, 5*time.Second); err != nil {
		Logger.Infof("error nats")
		return err
	}

	return nil
}

func (s *WeepiEvent) UpdateEvent(ctx context.Context, in *pb.Event) (*pb.EventReply, error) {
	_, err := uuid.FromString(in.GetUUID())
	if err != nil {
		return nil, errors.New("Erroneous id")
	}

	eventUUID := in.GetUUID()

	event := schemes.Event{}
	if s.db.Where("uuid = ?", eventUUID).First(&event).RecordNotFound() {
		Logger.Errorf("Failed to update event: %s does not exist in DB", eventUUID)
		return nil, errors.New("This event is not in db")
	}

	if err := s.validEventInfos(in); err != nil {
		if serr, ok := err.(*InternalValidateError); ok {
			Logger.WithError(serr.InternalErr).Error("internal validation process error")
		}
		Logger.WithError(err).Error("validation process failed")
		return nil, err
	}

	updatedEvent := &schemes.Event{
		Title:           in.GetTitle(),
		Private:         in.GetPrivate(),
		Sponsored:       in.GetSponsored(),
		StartDate:       time.Unix(in.GetStartDate().GetSeconds(), 0),
		EndDate:         time.Unix(in.GetEndDate().GetSeconds(), 0),
		Description:     in.GetDescription(),
		Latitude:        in.GetLatitude(),
		Longitude:       in.GetLongitude(),
		SportUUID:       uuid.FromStringOrNil(in.GetSportUUID()),
		MinParticipants: in.GetMinParticipants(),
		MaxParticipants: in.GetMaxParticipants(),
		OwnerUUID:       uuid.FromStringOrNil(in.GetOwnerUUID()),
	}
	if err = s.db.Model(&event).Updates(updatedEvent).Error; err != nil {
		Logger.Errorf("Updating event with UUID: %s, error: %s", eventUUID, err)

		return nil, errors.New("This event could not be updated")
	}

	err = s.updateInAlgolia(updatedEvent, eventUUID)

	return &pb.EventReply{
		Event: &pb.Event{
			UUID: event.UUID.String(),
		},
	}, nil
}
