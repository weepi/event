package main

import (
	"errors"
	"time"

	pb "gitlab.com/weepi/event/proto"
	"gitlab.com/weepi/event/schemes"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var (
	ErrUserUuidNotFound = errors.New("user UUID not found")
	ErrInvalidUuid      = errors.New("invalid user UUID")
	ErrInvalidDb        = errors.New("this event is not in db")
	ErrFullEvent        = errors.New("event participant list already full")
	ErrAlreadyInEvent   = errors.New("you are already in this event")
	ErrInvalidEventUUID = errors.New("erroneous id")
	ErrEventDetails     = errors.New("Could not save event details")
)

// Get UUID from context
func getValidUserUUID(ctx context.Context) (uuid.UUID, error) {
	meta, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return uuid.Nil, status.Errorf(codes.Unauthenticated, "missing context metadata")
	}
	if len(meta["useruuid"]) != 1 {
		//s.logger.Debug("could not find userUuid in meta")
		return uuid.Nil, ErrUserUuidNotFound
	}
	userUUID, err := uuid.FromString(meta["useruuid"][0])
	if err != nil {
		return uuid.Nil, ErrInvalidUuid
	}
	return userUUID, nil
}

func createPartList(partUuid uuid.UUID, eventUuid uuid.UUID, s WeepiEvent) error {

	eventPart := schemes.EventParticipant{}

	eventPart.ParticipantUUID = partUuid
	eventPart.EventUUID = eventUuid

	// Create the eventParticipant table entry
	err := s.db.Create(eventPart).Error
	if err != nil {
		Logger.WithError(err).Errorf("Inserting participant [%s] to event [%s] failed",
			eventPart.ParticipantUUID, eventPart.EventUUID)
		return ErrEventDetails
	}
	return nil
}

func (s *WeepiEvent) JoinEvent(ctx context.Context, in *pb.UserEvent) (*empty.Empty, error) {
	// Checking validity of uuid
	eventUUID, err := uuid.FromString(in.GetEventUUID())
	if err != nil {
		return nil, ErrInvalidEventUUID
	}
	userUUID, err := uuid.FromString(in.GetUserUUID())
	if err != nil {
		return nil, ErrInvalidEventUUID
	}

	// Getting event from database
	event := schemes.Event{}
	if s.db.Where("uuid = ?", eventUUID).First(&event).RecordNotFound() {
		Logger.WithError(ErrRecordNotFound).Errorf("Failed to get event: %s does not exist in DB", eventUUID)
		return nil, ErrInvalidDb
	}

	// Checking validity of user uuid
	if err != nil {
		Logger.WithError(err).Error("Failed to get user UUID: Invalid user UUID")
		return nil, ErrInvalidUuid
	}

	// Create the eventParticipant entry if it doesn't already exist and the event is not full
	if s.db.Where("participant_uuid = ? AND event_uuid = ?", userUUID, eventUUID).First(&schemes.EventParticipant{}).RecordNotFound() {
		count := 0
		s.db.Where("event_uuid = ?", eventUUID).Find(&schemes.EventParticipant{}).Count(&count)
		if count == int(event.MaxParticipants) {
			return nil, ErrFullEvent
		}
		err := createPartList(userUUID, event.UUID, *s)
		if err != nil {
			return nil, err
		}
		var ret error
		if err := s.msg.Request("algolia.event.join", &eventUUID, &ret, 5*time.Second); err != nil {
			Logger.WithError(err).Error("error requesting event join")
			return nil, err
		}
		if ret != nil {
			return nil, ret
		}
	} else {
		return nil, ErrAlreadyInEvent
	}

	return &empty.Empty{}, nil
}
