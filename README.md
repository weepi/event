# Weepi event !

The weepi event microservice was used by [Weepen](https://weepen.io/) to manage sports events.

The docker compose file in the [deploy repository](https://gitlab.com/weepi/deploy) was used to manage it.
