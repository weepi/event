package main

import (
	"fmt"

	"gitlab.com/weepi/common/db"
	"gitlab.com/weepi/common/utils"
	"gitlab.com/weepi/event/schemes"

	"github.com/nats-io/go-nats"
)

type WeepiEvent struct {
	db  *db.DB
	msg *nats.EncodedConn
}

func (we *WeepiEvent) registerMsgActions() error {
	// Delete events when user is deleted
	if _, err := we.msg.Subscribe("event.deleteUser", func(subj, reply string, userUUID string) {
		Logger.Debugf("I saw a message in event.deleteUser: '%v'", userUUID)
		// insert cred
		errb := we.deleteUserEvent(userUUID)
		var errMsg utils.Error
		if errb != nil {
			errMsg.Msg = errb.Error()
		}
		if err := we.msg.Publish(reply, &errMsg); err != nil {
			Logger.Error("publishing error")
		}
	}); err != nil {
		Logger.Error("delete user event subscribing failure")
		return err
	}
	return nil
}

func NewWeepiEvent() (*WeepiEvent, error) {
	// Db conn
	Logger.Infof("Attempting to connect to the database at %s...", Cfg.DBHost)
	d, err := db.New(&db.Config{
		Driver:   Cfg.DBDriver,
		Host:     Cfg.DBHost,
		User:     Cfg.DBUser,
		Name:     Cfg.DBName,
		SSLMode:  Cfg.DBSSLMode,
		Password: Cfg.DBPassword,
	}, OldLogger)
	if err != nil {
		return nil, err
	}
	d.LogMode(true)

	// Migrate the schema
	if err = d.AutoMigrate(&schemes.Event{}, &schemes.EventParticipant{}).Error; err != nil {
		return nil, err
	}

	Logger.Info("DB conn OK")

	// NATS conn
	dsn := fmt.Sprintf("nats://%s", Cfg.NATSAddr)
	nc, err := nats.Connect(dsn)
	if err != nil {
		Logger.WithError(err).Fatal("can't connect to NATS")
	}
	econn, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		Logger.WithError(err).Fatal("can't create encoded conn")
	}

	Logger.Info("NATS conn OK")

	return &WeepiEvent{
		db:  d,
		msg: econn,
	}, nil
}

func (s *WeepiEvent) Close() error {
	Logger.Info("Server is shutting down...")
	return s.db.Close()
}
