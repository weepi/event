package main

import (
	"errors"
	"time"

	algolia "gitlab.com/weepi/algolia/event"
	pb "gitlab.com/weepi/event/proto"

	"github.com/golang/protobuf/ptypes/empty"
	"github.com/satori/go.uuid"
	"gitlab.com/weepi/event/schemes"
	"golang.org/x/net/context"
)

var (
	ErrRecordNotFound    = errors.New("Record not found")
	ErrNotDeleted        = errors.New("This event could not be deleted")
	ErrNotDeletedAlgolia = errors.New("couldn't delete event in algolia")
)

func (s *WeepiEvent) deleteInAlgolia(UUID string) error {

	algoliaEvent := algolia.EventMsg{
		ObjectID: UUID,
	}

	var ret error
	Logger.Infof("before nats")
	if err := s.msg.Request("algolia.event.delete", &algoliaEvent, &ret, 5*time.Second); err != nil {
		Logger.Infof("error nats")
		return err
	}

	return nil
}

func (s *WeepiEvent) DeleteEvent(ctx context.Context, in *pb.UserEvent) (*empty.Empty, error) {

	eventUUID, err := uuid.FromString(in.GetEventUUID())

	if err != nil {
		return nil, errors.New("Erroneous id")
	}

	if _, err := uuid.FromString(in.GetUserUUID()); err != nil {
		return nil, errors.New("Erroneous id")
	}

	event := schemes.Event{}
	if s.db.Where("uuid = ? AND owner_uuid = ?", in.GetEventUUID(), in.GetUserUUID()).First(&event).RecordNotFound() {
		Logger.WithError(ErrRecordNotFound).Errorf("Failed to get event: %s does not exist in DB", eventUUID)
		return nil, errors.New("This event is not in db")
	}

	if err := s.db.Where("event_uuid = ?", in.GetEventUUID()).Delete(schemes.EventParticipant{}).Error; err != nil {
		Logger.WithError(err).Error("Could not unsubscribe some users from the event")
		return nil, err
	}

	if err := s.db.Where("uuid = ?", eventUUID).Delete(schemes.Event{}).Error; err != nil {
		Logger.WithError(err).Error(ErrNotDeleted)
		return nil, err
	}

	if err := s.deleteInAlgolia(in.GetEventUUID()); err != nil {
		Logger.WithError(err).Error(ErrNotDeletedAlgolia)
		return nil, err
	}

	return &empty.Empty{}, err

}
