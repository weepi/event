echo '{ "orderby":"uuid",
	"nbperpage":"3",
	"nbpage":"2",
	"private":"false",
	"sponsored":"true",
	"minDate":"2018-01-27T11:03:44.557Z",
	"sports":[
		{"UUID":"0fb5f842-f83b-4aab-98c4-00cb5381e842"},
		{"UUID":"8fb5f842-f83b-4aab-98c4-00cb5381e842"}
	]
      }' | polyglot --command=call --use_reflection=false --proto_discovery_root=$GOPATH/src/gitlab.com/weepi/event/proto --add_protoc_includes=/usr/include/,$GOPATH/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis --endpoint=127.0.0.1:1665 --full_method='.WeepiEvent/eventsInfos'
