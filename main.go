package main

import (
	"flag"
	"fmt"
	"net"
	"os"

	"gitlab.com/weepi/common/log"
	"gitlab.com/weepi/common/utils"
	pb "gitlab.com/weepi/event/proto"

	"github.com/Sirupsen/logrus"
	"github.com/caarlos0/env"
	"github.com/joho/godotenv"
	"google.golang.org/grpc"
)

type config struct {
	ListenAddr string `env:"LISTEN_ADDR,required"`
	DBDriver   string `env:"DB_DRIVER,required"`
	DBHost     string `env:"DB_HOST,required"`
	DBUser     string `env:"DB_USER,required"`
	DBName     string `env:"DB_NAME,required"`
	DBSSLMode  string `env:"DB_SSLMODE" envDefault:"disable"`
	DBPassword string `env:"DB_PASSWORD"`
	NATSAddr   string `env:"NATS_ADDR"`
	UseTLS     bool   `env:"USE_TLS" envDefault:"false"`
	TLSCrt     string `env:"TLS_CRT"`
	TLSKey     string `env:"TLS_KEY"`
}

const (
	configFilename = "config"
	tagName        = "env"
)

var (
	Logger    *logrus.Logger
	OldLogger *log.Logger
	Cfg       config
)

func init() {
	var helpFlag = flag.Bool("help", false, "display help")
	flag.Parse()
	if *helpFlag {
		help()
		os.Exit(0)
	}

	logrus.SetFormatter(&logrus.TextFormatter{
		TimestampFormat: "2006-01-02T15:04:05.000",
		FullTimestamp:   true,
	})

	var err error
	OldLogger, err = log.New()
	if err != nil {
		panic(err)
	}
	Logger = logrus.New()
}

func help() {
	fmt.Println("The following env vars can be set:")
	for _, v := range utils.GetStructTags(Cfg, tagName) {
		fmt.Printf("%s\n", v)
	}
}

func main() {
	// Init
	Logger.Info("[Server] Starting...")

	// Load env vars from file
	if err := godotenv.Load(configFilename); err != nil {
		Logger.WithError(err).Warn("could not read configuration from file")
	}
	// Get config from env vars
	if err := env.Parse(&Cfg); err != nil {
		Logger.WithError(err).Fatal("could not parse configuration")
	}

	// Create server
	srv, err := NewWeepiEvent()
	if err != nil {
		Logger.Fatalf("[Server] Creating server error : %s", err)
	}
	if err = srv.registerMsgActions(); err != nil {
		Logger.Fatal("could not register msg actions")
	}

	g := grpc.NewServer()
	pb.RegisterWeepiEventServer(g, srv)

	// Listen
	lis, err := net.Listen("tcp", Cfg.ListenAddr)
	if err != nil {
		Logger.Fatalf("[Server] Failed to listen on address %s: %s", Cfg.ListenAddr, err)
	}
	defer lis.Close()

	// Serve
	Logger.Infof("[Server] Started and listening on address %s", Cfg.ListenAddr)
	if err = g.Serve(lis); err != nil {
		Logger.WithError(err).Error("Stop server error")
	}
	Logger.Info("[Server] The server has been stopped")

	if err = srv.Close(); err != nil {
		Logger.Fatalf("[Server] Shutting down sports service error: %s", err)
	}
}
