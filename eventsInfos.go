package main

import (
	"errors"
	"fmt"
	"time"

	pbCom "gitlab.com/weepi/common/protobuf"
	pb "gitlab.com/weepi/event/proto"
	"gitlab.com/weepi/event/schemes"

	"github.com/golang/protobuf/ptypes/timestamp"
	"golang.org/x/net/context"
)

const (
	defaultOrderByEvents   = "start_date"
	defaultNbPerPageEvents = 5
	defaultNbPageEvents    = 1
)

func (s *WeepiEvent) castEvents(dbEvents []schemes.Event) ([]*pb.Event, error) {
	var pbEvents []*pb.Event
	for _, event := range dbEvents {
		var sport string
		if err := s.msg.Request("sport.getName", event.SportUUID.String(), &sport, 3*time.Second); err != nil {
			return nil, err
		}
		var nbParticipants uint32
		s.db.Where("event_uuid = ?", event.UUID.String()).Find(&schemes.EventParticipant{}).Count(&nbParticipants)
		pbEvents = append(pbEvents, &pb.Event{
			Title:     event.Title,
			UUID:      event.UUID.String(),
			Private:   event.Private,
			Sponsored: event.Sponsored,
			StartDate: &timestamp.Timestamp{
				Seconds: event.StartDate.Unix(),
			},
			EndDate: &timestamp.Timestamp{
				Seconds: event.EndDate.Unix(),
			},
			Description:     event.Description,
			Latitude:        event.Latitude,
			Longitude:       event.Longitude,
			SportUUID:       event.SportUUID.String(),
			Sport:           sport,
			MinParticipants: event.MinParticipants,
			MaxParticipants: event.MaxParticipants,
			NbParticipants:  nbParticipants,
			OwnerUUID:       event.OwnerUUID.String(),
		})
	}
	return pbEvents, nil
}

func convertSports(pbSports []*pb.UUID) []string {
	var strSports []string

	for _, pbSport := range pbSports {
		strSports = append(strSports, pbSport.UUID)
	}

	return strSports
}

func (s *WeepiEvent) EventsInfos(ctx context.Context, in *pb.ListRequest) (*pb.EventsReply, error) {
	if in.Orderby == "" {
		in.Orderby = defaultOrderByEvents
	}

	allowedOrderBy := []string{"uuid", "start_date", "end_date", "sport_uuid", "owner_uuid"}
	err := errors.New("Erroneous parameter : orderby (uuid, start_date, end_date, sport_uuid,owner_uuid)")
	for _, order := range allowedOrderBy {
		if in.Orderby == order {
			err = nil
		}
	}
	if err != nil {
		return nil, err
	}

	if in.Nbperpage == 0 {
		in.Nbperpage = defaultNbPerPageEvents
	}

	if in.Nbpage == 0 {
		in.Nbpage = defaultNbPageEvents
	}

	order := fmt.Sprintf("%s %s", in.Orderby, pbCom.Order_name[int32(in.Order)])
	offset := (in.Nbpage - 1) * in.Nbperpage

	events := []schemes.Event{}

	db := s.db.DB
	if pb.ListRequest_Tristate_name[int32(in.Private)] != "UNSPECIFIED" {
		db = db.Where("private = ?", pb.ListRequest_Tristate_name[int32(in.Private)])
	}
	if pb.ListRequest_Tristate_name[int32(in.Sponsored)] != "UNSPECIFIED" {
		db = db.Where("sponsored = ?", pb.ListRequest_Tristate_name[int32(in.Sponsored)])
	}
	if in.MinDate != nil {
		db = db.Where("EXTRACT(EPOCH FROM start_date) > ?", in.MinDate.GetSeconds())
	}
	if in.MaxDate != nil {
		db = db.Where("EXTRACT(EPOCH FROM start_date) < ?", in.MaxDate.GetSeconds())
	}
	if len(in.Sports) != 0 {
		db = db.Where("sport_uuid in (?)", convertSports(in.Sports))
	}
	if in.Owner != nil {
		db = db.Where("owner_uuid = ?", in.Owner.UUID)
	}
	if in.User != nil {
		db = db.Joins("JOIN event_participants ON events.uuid = event_participants.event_uuid AND event_participants.participant_uuid = ?", in.User.UUID)
	}

	db.Order(order).Offset(offset).Limit(in.Nbperpage).Find(&events)

	var total uint32
	s.db.Model(&schemes.Event{}).Count(&total)

	nbPages := total / in.Nbperpage
	if total%in.Nbperpage != 0 {
		nbPages += 1
	}

	pagination := pb.Pagination{
		Nbtotal:   uint64(total),
		Nbperpage: in.Nbperpage,
		Page:      in.Nbpage,
		Nbpages:   nbPages,
	}
	pbEvents, err := s.castEvents(events)
	if err != nil {
		return nil, err
	}

	return &pb.EventsReply{
		Pagination: &pagination,
		Events:     pbEvents,
	}, nil
}
