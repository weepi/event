package main

import (
	"errors"

	pb "gitlab.com/weepi/event/proto"
	"gitlab.com/weepi/event/schemes"

	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
)

func (s *WeepiEvent) EventParticipants(ctx context.Context, in *pb.UUID) (*pb.Participants, error) {
	eventUUID, err := uuid.FromString(in.GetUUID())
	if err != nil {
		return nil, errors.New("Erroneous UUID")
	}

	event := schemes.Event{}
	if s.db.Where("uuid = ?", in.GetUUID()).First(&event).RecordNotFound() {
		Logger.WithError(ErrRecordNotFound).Errorf("Failed to get event: %s does not exist in DB", eventUUID)
		return nil, errors.New("This event is not in db")
	}

	s.db.Model(&event).Related(&event.EventParticipant, "EventParticipant")

	var reply pb.Participants
	for _, participant := range event.EventParticipant {
		reply.Participants = append(reply.Participants, &pb.UUID{
			UUID: participant.ParticipantUUID.String(),
		})
	}

	return &reply, nil
}
