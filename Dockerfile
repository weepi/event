FROM golang:alpine

MAINTAINER Florian Videau <jonathan.biteau@epitech.eu>

ENV APP_PATH=$GOPATH/src/gitlab.com/weepi/event

ADD . $APP_PATH
WORKDIR $APP_PATH

RUN apk update
RUN apk add make git

RUN make install

ENTRYPOINT $GOPATH/bin/event
