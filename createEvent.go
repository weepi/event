package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	algolia "gitlab.com/weepi/algolia/event"
	pb "gitlab.com/weepi/event/proto"
	"gitlab.com/weepi/event/schemes"

	"github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
)

const (
	MinDescLength           = 15
	MaxDescLength           = 500
	MinTitleLength          = 3
	MaxTitleLength          = 20
	MinPartNbr              = 2
	publicMattermostChannel = "O"
)

var (
	ErrDescLength       = fmt.Errorf("Description length must be between %d and %d", MinDescLength, MaxDescLength)
	ErrTitleLength      = fmt.Errorf("Title length must be between %d and %d", MinTitleLength, MaxTitleLength)
	ErrMinPartNbr       = fmt.Errorf("The required number of participants must be more than %d", MinPartNbr)
	ErrPartNbrDiff      = fmt.Errorf("The minimum number of participants must be less than the maximum")
	ErrInvalidStartDate = errors.New("The start date is invalid")
	ErrInvalidEndDate   = errors.New("The end date is invalid")
	ErrInvalidDiffDates = errors.New("The start date is after the end date")
	ErrInvalidOwner     = errors.New("The owner uuid %s is invalid")
	ErrInvalidSport     = errors.New("The sport uuid is invalid")
	ErrNonExistantSport = errors.New("The sport does not exist")
	ErrAlgolia          = errors.New("Couldn't create event in algolia")
	ErrMattermostCreate = errors.New("Couldn't create mattermost's channel")
	ErrNoTeam           = errors.New("Could't find team on mattermost server")
)

type InternalValidateError struct {
	err         error
	InternalErr error
}

func (e *InternalValidateError) Error() string {
	return e.err.Error()
}

func (s *WeepiEvent) validEventInfos(in *pb.Event) error {
	// Check Desc Length
	if len(in.GetDescription()) < MinDescLength || len(in.GetDescription()) > MaxDescLength {
		return ErrDescLength
	}

	// Check title Length
	if len(in.GetTitle()) < MinTitleLength || len(in.GetTitle()) > MaxTitleLength {
		return ErrTitleLength
	}

	// Check Part Nbr
	minPart := in.GetMinParticipants()
	maxPart := in.GetMaxParticipants()
	if minPart < MinPartNbr {
		return ErrMinPartNbr
	}
	if minPart > maxPart {
		return ErrPartNbrDiff
	}

	// Check start and end dates
	startDate := in.GetStartDate().GetSeconds()
	endDate := in.GetEndDate().GetSeconds()

	if startDate == 0 {
		return ErrInvalidStartDate
	}
	if endDate == 0 {
		return ErrInvalidEndDate
	}
	if startDate >= endDate {
		return ErrInvalidDiffDates
	}

	// Verifying validity of uuid's
	if _, errSp := uuid.FromString(in.GetSportUUID()); errSp != nil {
		return ErrInvalidSport
	}
	if _, errOw := uuid.FromString(in.GetOwnerUUID()); errOw != nil {
		return ErrInvalidOwner
	}

	// Checking sport UUID
	var sportExists bool
	if err := s.msg.Request("sport.exists", in.GetSportUUID(), &sportExists, 3*time.Second); err != nil {
		Logger.Error("Error getting existence of sport")
		return err
	}
	if !sportExists {
		return ErrNonExistantSport
	}

	// Checking locations values
	latitude := in.GetLatitude()
	longitude := in.GetLongitude()

	if errLon, errLat := validation.Validate(strconv.FormatFloat(longitude, 'f', -1, 64), is.Longitude),
		validation.Validate(strconv.FormatFloat(latitude, 'f', -1, 64), is.Latitude); errLon != nil || errLat != nil {
		return fmt.Errorf(strings.Join([]string{errLon.Error(), errLat.Error()}, ""))
	}

	return nil
}

func (s *WeepiEvent) createInAlgolia(event *schemes.Event) error {
	coords := algolia.Coords{
		Lat: event.Latitude,
		Lng: event.Longitude,
	}

	algoliaEvent := algolia.EventMsg{
		Title:            event.Title,
		ObjectID:         event.UUID.String(),
		Private:          event.Private,
		Sponsored:        event.Sponsored,
		StartDate:        event.StartDate,
		EndDate:          event.EndDate,
		EndDateTimestamp: event.EndDateTimestamp,
		Description:      event.Description,
		Geoloc:           coords,
		SportUUID:        event.SportUUID.String(),
		MinParticipants:  event.MinParticipants,
		MaxParticipants:  event.MaxParticipants,
		OwnerUUID:        event.OwnerUUID.String(),
	}

	var ret error
	if err := s.msg.Request("algolia.event.create", &algoliaEvent, &ret, 5*time.Second); err != nil {
		Logger.Infof("error nats")
		return err
	}

	return nil
}

func (s *WeepiEvent) CreateEvent(ctx context.Context, in *pb.Event) (*pb.UUID, error) {
	Logger.Infof("Creating event [%+v]...", in)

	// Check data
	if err := s.validEventInfos(in); err != nil {
		if serr, ok := err.(*InternalValidateError); ok {
			Logger.WithError(serr.InternalErr).Error("internal validation process error")
		}
		Logger.WithError(err).Error("validation process failed")
		return nil, err
	}

	// Fill Event struct
	newEvent := &schemes.Event{
		Title:            in.GetTitle(),
		Private:          in.GetPrivate(),
		Sponsored:        in.GetSponsored(),
		StartDate:        time.Unix(in.GetStartDate().GetSeconds(), 0),
		EndDate:          time.Unix(in.GetEndDate().GetSeconds(), 0),
		EndDateTimestamp: uint64(in.GetEndDate().GetSeconds()),
		Description:      in.GetDescription(),
		Latitude:         in.GetLatitude(),
		Longitude:        in.GetLongitude(),
		SportUUID:        uuid.FromStringOrNil(in.GetSportUUID()),
		MinParticipants:  in.GetMinParticipants(),
		MaxParticipants:  in.GetMaxParticipants(),
		OwnerUUID:        uuid.FromStringOrNil(in.GetOwnerUUID()),
	}

	// Inserting event in db
	err := s.db.Create(newEvent).Error
	if err != nil {
		Logger.WithError(err).Errorf("Inserting event [%s] error",
			newEvent.Description)

		return nil, errors.New("Could not save event details")
	}

	if err := s.createInAlgolia(newEvent); err != nil {
		Logger.WithError(err).Error(ErrAlgolia)
		return nil, ErrAlgolia
	}

	// Making the owner join his own event
	ownerUuid := uuid.FromStringOrNil(in.GetOwnerUUID())
	if err := createPartList(ownerUuid, newEvent.UUID, *s); err != nil {
		return nil, err
	}

	return &pb.UUID{
		UUID: newEvent.UUID.String(),
	}, nil
}
