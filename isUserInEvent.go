package main

import (
	pb "gitlab.com/weepi/event/proto"
	"gitlab.com/weepi/event/schemes"

	"github.com/satori/go.uuid"
	"golang.org/x/net/context"
)

func (s *WeepiEvent) IsUserInEvent(ctx context.Context, in *pb.UserEvent) (*pb.Bool, error) {
	// Checking validity of uuid
	eventUUID, err := uuid.FromString(in.GetEventUUID())
	if err != nil {
		return nil, ErrInvalidEventUUID
	}
	userUUID, err := uuid.FromString(in.GetUserUUID())
	if err != nil {
		return nil, ErrInvalidUuid
	}

	// Checking existence of combination in event db
	if s.db.Where("participant_uuid = ? AND event_uuid = ?", userUUID, eventUUID).First(&schemes.EventParticipant{}).RecordNotFound() {
		return &pb.Bool{
			IsInEvent: false,
		}, nil
	}

	return &pb.Bool{
		IsInEvent: true,
	}, nil
}
