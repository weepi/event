package main

import (
	"github.com/satori/go.uuid"
	pb "gitlab.com/weepi/event/proto"
	"gitlab.com/weepi/event/schemes"
	"golang.org/x/net/context"
)

func castEventsParticipants(dbEvents []schemes.EventParticipant) []*pb.EventParticipant {
	var pbEventsParticipants []*pb.EventParticipant
	for _, event := range dbEvents {
		pbEventsParticipants = append(pbEventsParticipants, &pb.EventParticipant{
			EventUUID: event.EventUUID.String(),
			UserUUID:  event.ParticipantUUID.String(),
		})
	}
	return pbEventsParticipants
}

func (s *WeepiEvent) EventsForUser(ctx context.Context, in *pb.UUID) (*pb.EventsByUser, error) {
	eventsParticipants := []schemes.EventParticipant{}

	userUUID, err := uuid.FromString(in.GetUUID())
	if err != nil {
		return nil, ErrInvalidEventUUID
	}

	if err := s.db.Model(&schemes.EventParticipant{}).
		Where("participant_uuid = ?", userUUID).Find(&eventsParticipants).Error; err != nil {
		Logger.WithError(err).Error(ErrUserUuidNotFound)
		return nil, err
	}

	pbEvents := castEventsParticipants(eventsParticipants)

	return &pb.EventsByUser{
		Events: pbEvents,
	}, nil
}
