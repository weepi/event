package db

import (
	"fmt"

	"gitlab.com/weepi/common/log"
	"strings"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var Logger *log.Logger

type Config struct {
	Driver   string
	Host     string
	User     string
	Name     string
	SSLMode  string
	Password string
}

type DB struct {
	*gorm.DB
}

type DBLogger struct{}

func (l DBLogger) Println(in ...interface{}) {
	var out []string
	for _, v := range in {
		val, ok := v.(string)
		if !ok {
			val = fmt.Sprintf("%v", v)
		}
		out = append(out, strings.TrimSpace(val))
	}
	Logger.Debug(strings.Join(out, " "))
}

func New(c *Config, l *log.Logger) (*DB, error) {
	Logger = l
	dsn := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=%s password=%s",
		c.Host, c.User, c.Name, c.SSLMode, c.Password)
	db, err := gorm.Open(c.Driver, dsn)
	if err != nil {
		return nil, err
	}

	// Set our logger
	db.SetLogger(gorm.Logger{DBLogger{}})

	return &DB{db}, nil
}
