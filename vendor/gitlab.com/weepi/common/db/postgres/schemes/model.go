package schemes

import (
	"time"

	"github.com/satori/go.uuid"
)

type Model struct {
	UUID      uuid.UUID `gorm:"primary_key;type:uuid;default:gen_random_uuid()"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
