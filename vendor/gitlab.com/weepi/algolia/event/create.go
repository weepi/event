package event

import (
	"errors"
	"strconv"
	"time"

	"github.com/algolia/algoliasearch-client-go/algoliasearch"
	"github.com/fatih/structs"
	"github.com/nats-io/go-nats"
	"golang.org/x/net/context"
	"googlemaps.github.io/maps"
)

func getLocation(coords Coords, mapsClient *maps.Client) (*Location, error) {
	r := &maps.GeocodingRequest{
		LatLng: &maps.LatLng{
			Lat: coords.Lat,
			Lng: coords.Lng,
		},
	}

	address, err := mapsClient.ReverseGeocode(context.Background(), r)
	if err != nil {
		return nil, err
	}

	if len(address) == 0 {
		return nil, errors.New("no address found for given latitude/longitude")
	}
	addressComponents := address[0].AddressComponents

	var city, country string
	var zip uint64

	for _, addressComponent := range addressComponents {
		for _, compoType := range addressComponent.Types {
			if compoType == "locality" {
				city = addressComponent.LongName
			} else if compoType == "country" {
				country = addressComponent.LongName
			} else if compoType == "postal_code" {
				zip, _ = strconv.ParseUint(addressComponent.LongName, 10, 64)
			}
		}
	}

	return &Location{
		Geoloc: Coords{
			Lat: coords.Lat,
			Lng: coords.Lng,
		},
		City:    city,
		Zip:     zip,
		Country: country,
	}, nil
}

func Create(eventMsg EventMsg, algoliaIndex algoliasearch.Index, mapsClient *maps.Client, natsClient *nats.EncodedConn) error {

	location, err := getLocation(eventMsg.Geoloc, mapsClient)
	if err != nil {
		return err
	}

	eventMsg.Location = *location
	eventMsg.Reservation.Currency = "€"
	eventMsg.NbParticipants = 1

	if err := natsClient.Request("sport.getName", eventMsg.SportUUID, &eventMsg.Sport, 3*time.Second); err != nil {
		return err
	}

	if _, err := algoliaIndex.AddObject(structs.Map(eventMsg)); err != nil {
		return err
	}

	return nil
}
