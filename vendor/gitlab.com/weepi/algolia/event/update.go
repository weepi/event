package event

import (
	"time"

	"github.com/algolia/algoliasearch-client-go/algoliasearch"
	"github.com/fatih/structs"
	"github.com/nats-io/go-nats"
	"googlemaps.github.io/maps"
)

func Update(eventMsg EventMsg, algoliaIndex algoliasearch.Index, mapsClient *maps.Client, natsClient *nats.EncodedConn) error {

	location, err := getLocation(eventMsg.Geoloc, mapsClient)
	if err != nil {
		return err
	}

	eventMsg.Location = *location
	//	eventMsg.Reservation.Currency = "€"
	//	eventMsg.NbParticipants = 1

	if err := natsClient.Request("sport.getName", eventMsg.SportUUID, &eventMsg.Sport, 3*time.Second); err != nil {
		return err
	}

	if _, err := algoliaIndex.PartialUpdateObject(structs.Map(eventMsg)); err != nil {
		return err
	}

	return nil
}
