package event

import (
	"github.com/algolia/algoliasearch-client-go/algoliasearch"
)

func Delete(eventMsg EventMsg, algoliaIndex algoliasearch.Index) error {

	if _, err := algoliaIndex.DeleteObject(eventMsg.ObjectID); err != nil {
		return err
	}

	return nil
}
